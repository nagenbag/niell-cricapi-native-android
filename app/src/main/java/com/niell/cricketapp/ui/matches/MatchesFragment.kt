package com.niell.cricketapp.ui.matches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.niell.cricketapp.R
import com.niell.cricketapp.helpers.mainActivity
import com.niell.cricketapp.ui.matchdetails.MatchDetailsFragment
import com.niell.cricketapp.ui.matchdetails.NewMatchCardModel
import com.niell.cricketapp.ui.matchdetails.OldMatchCardModel
import kotlinx.android.synthetic.main.fr_matches.*


class MatchesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fr_matches, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivity?.matchesViewModel?.newMatches?.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                newMatch1.visibility = View.VISIBLE
                newMatch2.visibility = View.VISIBLE
                newMatch3.visibility = View.VISIBLE
                newMatchesError.visibility = View.GONE

                newMatch1.populateBasicMatchView(it.matches?.get(0))
                newMatch2.populateBasicMatchView(it.matches?.get(1))
                newMatch3.populateBasicMatchView(it.matches?.get(2))
            } else {
                newMatch1.visibility = View.GONE
                newMatch2.visibility = View.GONE
                newMatch3.visibility = View.GONE
                newMatchesError.visibility = View.VISIBLE
            }
        })

        mainActivity?.matchesViewModel?.oldMatches?.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                oldMatchesError.visibility = View.GONE
                oldMatch1.visibility = View.VISIBLE
                oldMatch2.visibility = View.VISIBLE
                oldMatch3.visibility = View.VISIBLE

                oldMatch1.populateBasicMatchView(it.data?.get(0))
                oldMatch2.populateBasicMatchView(it.data?.get(1))
                oldMatch3.populateBasicMatchView(it.data?.get(2))
            } else {
                oldMatch1.visibility = View.GONE
                oldMatch2.visibility = View.GONE
                oldMatch3.visibility = View.GONE
                oldMatchesError.visibility = View.VISIBLE
            }
        })

        btnMoreNewMatches.setOnClickListener {
            val newMatches = mainActivity?.matchesViewModel?.newMatches?.value
            if (newMatches?.matches?.count() ?: 0 > 0) {
                mainActivity?.matchDetailsViewModel?.matches = newMatches!!.matches!!.map {
                    NewMatchCardModel(it)
                }

                mainActivity?.navigateToFragment(MatchDetailsFragment())
            } else {
                showErrorToast()
            }
        }

        btnMoreOldMatches.setOnClickListener {
            val oldMatches = mainActivity?.matchesViewModel?.oldMatches?.value
            if (oldMatches?.data?.count() ?: 0 > 0) {
                mainActivity?.matchDetailsViewModel?.matches = oldMatches!!.data!!.map {
                    OldMatchCardModel(it)
                }

                mainActivity?.navigateToFragment(MatchDetailsFragment())
            } else {
                showErrorToast()
            }
        }
    }

    private fun showErrorToast() {
        val toast = Toast.makeText(context, "No data to view", Toast.LENGTH_LONG)
        toast.show()
    }
}