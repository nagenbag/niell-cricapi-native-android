package com.niell.cricketapp.ui.players

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.niell.cricketapp.datastores.CricApiDataStore
import com.niell.cricketapp.models.PlayerFinderDTO
import com.niell.cricketapp.models.PlayerStatisticsDTO
import com.niell.cricketapp.ui.calendarmatches.CalendarMatchCardModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class PlayersViewModel : ViewModel(), CoroutineScope {

    val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val _players = MutableLiveData<PlayerFinderDTO?>()
    private val _playerInfo = MutableLiveData<PlayerStatisticsDTO?>()

    val players: LiveData<PlayerFinderDTO?> = _players
    val playerInfo: LiveData<PlayerStatisticsDTO?> = _playerInfo

    fun setPlayers(playerName: String) {
        launch {
            val res = runCatching {
                CricApiDataStore().getFindPlayer(playerName)
            }

            res.onSuccess { dto ->
                _players.value = dto
            }

            res.onFailure {
                _players.value = null
            }
        }
    }

    fun setPlayerInfo(playerID: String) {
        launch {
            val res = runCatching {
                CricApiDataStore().getPlayerScore(playerID)
            }

            res.onSuccess { dto ->
                _playerInfo.value = dto
            }

            res.onFailure {
                _playerInfo.value = null
            }
        }
    }
}