package com.niell.cricketapp.ui.matches

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.niell.cricketapp.datastores.CricApiDataStore
import com.niell.cricketapp.models.NewMatchesDTO
import com.niell.cricketapp.models.OldMatchesDTO
import com.niell.cricketapp.ui.calendarmatches.CalendarMatchCardModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MatchesViewModel : ViewModel(), CoroutineScope {

    val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val _newMatches = MutableLiveData<NewMatchesDTO?>().apply {
        launch {
            val res = runCatching {
                CricApiDataStore().getNewMatches()
            }

            res.onSuccess { dto->
                value = dto
            }

            res.onFailure {
                value = null
            }
        }
    }


    private val _oldMatches = MutableLiveData<OldMatchesDTO?>().apply {
        launch {
            val res = runCatching {
                CricApiDataStore().getOldMatches()
            }

            res.onSuccess { dto ->
                value = dto
            }

            res.onFailure {
                value = null
            }
        }
    }

    val newMatches: LiveData<NewMatchesDTO?> = _newMatches
    val oldMatches: LiveData<OldMatchesDTO?> = _oldMatches
}