package com.niell.cricketapp.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.niell.cricketapp.R
import com.niell.cricketapp.adaptors.ViewPagerAdaptor
import com.niell.cricketapp.ui.calendarmatches.CalendarMatchesFragment
import com.niell.cricketapp.ui.matches.MatchesFragment
import com.niell.cricketapp.ui.players.PlayersFragment
import kotlinx.android.synthetic.main.fr_dashboard.*

class DashboardFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fr_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adaptor =
            ViewPagerAdaptor(childFragmentManager)
        adaptor.addFragments(
            listOf(
                MatchesFragment(),
                CalendarMatchesFragment(),
                PlayersFragment()
            )
        )

        viewPager.adapter = adaptor

        setNavigation()
    }

    private fun setNavigation() {
        navigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    // Respond to navigation item 1 click
                    viewPager.setCurrentItem(0, true)
                    true
                }
                R.id.navigation_dashboard -> {
                    viewPager.setCurrentItem(1, true)
                    // Respond to navigation item 2 click
                    true
                }
                R.id.navigation_players -> {
                    viewPager.setCurrentItem(2, true)
                    // Respond to navigation item 2 click
                    true
                }
                else -> false
            }
        }
    }
}

