package com.niell.cricketapp.ui.players

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.niell.cricketapp.R
import com.niell.cricketapp.adaptors.GridViewAdaptor
import com.niell.cricketapp.adaptors.GridViewItemModel
import com.niell.cricketapp.helpers.mainActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fr_players.*


class PlayersFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fr_players, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivity?.playerViewModel?.players?.observe(viewLifecycleOwner, Observer {
            if (it?.data != null) {
                gridView.visibility = View.VISIBLE
                playerData.visibility = View.GONE
                gridView.adapter = GridViewAdaptor(
                    it.data.map { player ->
                        GridViewItemModel(
                            player.pid,
                            player.fullname
                        ) { playerID ->
                            viewPlayerData(playerID)
                        }
                    }
                )
            } else {
                basicErrorView.visibility = View.VISIBLE
            }
        })

        mainActivity?.playerViewModel?.playerInfo?.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                gridView.visibility = View.GONE
                playerData.visibility = View.VISIBLE

                Picasso.with(context)
                    .load(it.imageURL)
                    .placeholder(R.drawable.ic_player) //optional
                    .into(imgPlayer)

                txtPlayerName.text = it.name
                txtPlayingRole.text = it.playingRole
                txtCountry.text = it.country
                txtBio.text = it.profile

            } else {
                basicErrorView.visibility = View.VISIBLE
            }
        })

        btnSearch.setOnClickListener {
            basicErrorView.visibility = View.GONE
            mainActivity?.playerViewModel?.setPlayers(edtPlayerSearch.editText?.text.toString())
        }
    }

    private fun viewPlayerData(playerID: String?) {
        if (playerID != null) {
            basicErrorView.visibility = View.GONE
            mainActivity?.playerViewModel?.setPlayerInfo(playerID)
        } else {
            basicErrorView.visibility = View.VISIBLE
        }
    }
}