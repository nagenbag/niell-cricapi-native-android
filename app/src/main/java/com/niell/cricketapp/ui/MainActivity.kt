package com.niell.cricketapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.niell.cricketapp.R
import com.niell.cricketapp.ui.calendarmatches.CalendarMatchesViewModel
import com.niell.cricketapp.ui.dashboard.DashboardFragment
import com.niell.cricketapp.ui.matchdetails.MatchDetailsViewModel
import com.niell.cricketapp.ui.matches.MatchesViewModel
import com.niell.cricketapp.ui.players.PlayersViewModel

class MainActivity : AppCompatActivity() {
    lateinit var matchDetailsViewModel: MatchDetailsViewModel
    lateinit var calendarMatchesViewModel: CalendarMatchesViewModel
    lateinit var playerViewModel: PlayersViewModel
    lateinit var matchesViewModel: MatchesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setViewModels()

        val dashboardFragment = DashboardFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.rootLayout,
                dashboardFragment,
                dashboardFragment.tag
            )
            .commitAllowingStateLoss()
    }

    private fun setViewModels() {
        matchesViewModel = ViewModelProvider(this).get(MatchesViewModel::class.java)
        matchDetailsViewModel = ViewModelProvider(this).get(MatchDetailsViewModel::class.java)
        calendarMatchesViewModel = ViewModelProvider(this).get(CalendarMatchesViewModel::class.java)
        playerViewModel = ViewModelProvider(this).get(PlayersViewModel::class.java)
    }

    fun navigateToFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .add(
                R.id.rootLayout,
                fragment,
                fragment.tag
            )
            .commitAllowingStateLoss()


    }
}