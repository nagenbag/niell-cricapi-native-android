package com.niell.cricketapp.ui.matchdetails

import com.niell.cricketapp.helpers.BaseCardModel
import androidx.lifecycle.ViewModel

class MatchDetailsViewModel : ViewModel() {
    var matches: List<BaseCardModel> = listOf()
}