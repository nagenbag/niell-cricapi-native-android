package com.niell.cricketapp.ui.calendarmatches

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.niell.cricketapp.R
import com.niell.cricketapp.helpers.BaseCardModel
import com.niell.cricketapp.helpers.BaseViewHolder
import kotlinx.android.synthetic.main.vh_calendar_match.view.*

data class CalendarMatchCardModel(
    val date: String,
    val name: String
) : BaseCardModel(CardType.CALENDAR_MATCH)

class CalendarMatchViewholder(itemView: View) : BaseViewHolder(itemView) {
    companion object {
        fun getInstance(
            inflater: LayoutInflater,
            parent: ViewGroup
        ): CalendarMatchViewholder =
            CalendarMatchViewholder(
                inflater.inflate(
                    R.layout.vh_calendar_match,
                    parent,
                    false
                )
            )
    }

    var model: CalendarMatchCardModel? = null

    override fun bind(item: BaseCardModel) {
        super.bind(item)
        model = item as? CalendarMatchCardModel
    }

    override fun attach() {
        super.attach()
        model?.let {
            itemView.txtDate.text = it.date
            itemView.txtName.text = it.name
        }
    }
}