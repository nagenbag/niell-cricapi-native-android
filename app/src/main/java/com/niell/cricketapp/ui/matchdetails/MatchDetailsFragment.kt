package com.niell.cricketapp.ui.matchdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.niell.cricketapp.R
import com.niell.cricketapp.helpers.mainActivity
import com.niell.cricketapp.ui.MainActivity
import kotlinx.android.synthetic.main.fr_calendar_matches.*
import kotlinx.android.synthetic.main.fr_calendar_matches.recyclerView
import kotlinx.android.synthetic.main.fr_match_details.*
import kotlinx.android.synthetic.main.fr_matches.*

class MatchDetailsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return  inflater.inflate(R.layout.fr_match_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.updateList(
            mainActivity?.matchDetailsViewModel?.matches
        )
    }
}