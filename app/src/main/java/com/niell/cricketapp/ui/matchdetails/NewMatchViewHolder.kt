package com.niell.cricketapp.ui.matchdetails

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.niell.cricketapp.R
import com.niell.cricketapp.datastores.CricApiDataStore
import com.niell.cricketapp.helpers.BaseCardModel
import com.niell.cricketapp.helpers.BaseViewHolder
import com.niell.cricketapp.models.NewMatchesDTO
import kotlinx.android.synthetic.main.vh_new_match_detail.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

data class NewMatchCardModel(
    val data: NewMatchesDTO.Match
) : BaseCardModel(CardType.NEW_MATCH)

class NewMatchesViewHolder(itemView: View) : BaseViewHolder(itemView), CoroutineScope {
    companion object {
        fun getInstance(
            inflater: LayoutInflater,
            parent: ViewGroup
        ): NewMatchesViewHolder =
            NewMatchesViewHolder(
                inflater.inflate(
                    R.layout.vh_new_match_detail,
                    parent,
                    false
                )
            )
    }

    val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    var model: NewMatchCardModel? = null

    override fun bind(item: BaseCardModel) {
        super.bind(item)
        model = item as? NewMatchCardModel
    }

    override fun attach() {
        super.attach()

        model?.let {model->
            itemView.basicMatchInfo.populateBasicMatchView(model.data)

            itemView.btnScore.setOnClickListener {
                launch {
                    val res = runCatching {
                        CricApiDataStore().getCricketScore(model.data.uniqueID.toString())
                    }

                    res.onSuccess { dto ->
                        itemView.matchDetailView.populateMatchDetailView(dto)
                    }

                    res.onFailure {
                        itemView.matchDetailView.populateMatchDetailView(null)
                    }
                }
            }
        }
    }

    override fun detach() {
        job.cancel()
        super.detach()
    }
}