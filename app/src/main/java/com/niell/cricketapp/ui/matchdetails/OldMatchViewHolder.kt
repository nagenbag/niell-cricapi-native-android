package com.niell.cricketapp.ui.matchdetails

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.niell.cricketapp.R
import com.niell.cricketapp.datastores.CricApiDataStore
import com.niell.cricketapp.helpers.BaseCardModel
import com.niell.cricketapp.helpers.BaseViewHolder
import com.niell.cricketapp.models.OldMatchesDTO
import kotlinx.android.synthetic.main.vh_old_match_detail.view.basicMatchInfo
import kotlinx.android.synthetic.main.vh_old_match_detail.view.btnScore
import kotlinx.android.synthetic.main.vh_old_match_detail.view.matchDetailView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

data class OldMatchCardModel(
    val data: OldMatchesDTO.Match
) : BaseCardModel(CardType.OLD_MATCH)

class OldMatchViewHolder(itemView: View) : BaseViewHolder(itemView), CoroutineScope {
    companion object {
        fun getInstance(
            inflater: LayoutInflater,
            parent: ViewGroup
        ): OldMatchViewHolder =
            OldMatchViewHolder(
                inflater.inflate(
                    R.layout.vh_old_match_detail,
                    parent,
                    false
                )
            )
    }

    val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    var model: OldMatchCardModel? = null

    override fun bind(item: BaseCardModel) {
        super.bind(item)
        model = item as? OldMatchCardModel
    }

    override fun attach() {
        super.attach()

        model?.let { model->
            itemView.basicMatchInfo.populateBasicMatchView(model.data)

            itemView.btnScore.setOnClickListener {
                launch {
                    val res = runCatching {
                        CricApiDataStore().getCricketScore(model.data.uniqueID.toString())
                    }

                    res.onSuccess { dto ->
                        itemView.matchDetailView.populateMatchDetailView(dto)
                    }

                    res.onFailure {
                        itemView.matchDetailView.populateMatchDetailView(null)
                    }
                }
            }
        }
    }


    override fun detach() {
        job.cancel()
        super.detach()
    }
}