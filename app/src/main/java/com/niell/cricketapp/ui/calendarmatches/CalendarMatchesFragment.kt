package com.niell.cricketapp.ui.calendarmatches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.niell.cricketapp.R
import com.niell.cricketapp.ui.MainActivity
import kotlinx.android.synthetic.main.fr_calendar_matches.*
import kotlinx.android.synthetic.main.fr_matches.*

class CalendarMatchesFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fr_calendar_matches, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? MainActivity)?.let { mainActivity ->
            mainActivity.calendarMatchesViewModel.calendarMatchData.observe(
                viewLifecycleOwner,
                Observer {
                    if (it.count() > 0) {
                        basicErrorView.visibility = View.GONE
                        recyclerView.updateList(it)
                    } else {
                        basicErrorView.visibility = View.VISIBLE
                    }
                })
        }
    }
}