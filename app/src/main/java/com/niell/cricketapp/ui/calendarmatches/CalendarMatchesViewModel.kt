package com.niell.cricketapp.ui.calendarmatches

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.niell.cricketapp.datastores.CricApiDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class CalendarMatchesViewModel : ViewModel(), CoroutineScope {
    val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val _calendarMatchData = MutableLiveData<List<CalendarMatchCardModel>>().apply {
        launch {

            val res = runCatching {
                CricApiDataStore().getMatchCalendar()
            }

            res.onSuccess { dto ->
                val displayDateFormat = SimpleDateFormat("dd MMM yyyy hh:mm", Locale.getDefault())

                value = dto.data?.map { matchItem ->
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:m", Locale.getDefault())
                    val date = dateFormat.parse(matchItem.date ?: "")
                    CalendarMatchCardModel(
                        displayDateFormat.format(date),
                        matchItem.name ?: ""
                    )
                }
            }

            res.onFailure {
                value = listOf()
            }
        }
    }

    val calendarMatchData: LiveData<List<CalendarMatchCardModel>> = _calendarMatchData
}