package com.niell.cricketapp.services

import com.niell.cricketapp.models.*
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CricApi {

    @GET("matches")
    suspend fun requestNewMatches(
        @Query("apikey") apiKey: String
    ) : NewMatchesDTO

    @GET("cricket")
    suspend fun requestOldMatches(
        @Query("apikey") apiKey: String
    ): OldMatchesDTO

    @GET("matchCalendar")
    suspend fun requestMatchCalendar(
        @Query("apikey") apiKey: String
    ): MatchCalendarDTO

    @GET("cricketScore")
    suspend fun requestCricketScore(
        @Query("apikey") apiKey: String,
        @Query("unique_id") matchID: String?
    ): CricketScoreDTO
    
    @GET("playerStats")
    suspend fun requestPlayerScore(
        @Query("apikey") apiKey: String,
        @Query("pid") playerID: String?
    ): PlayerStatisticsDTO
    
    @GET("playerFinder")
    suspend fun requestFindPlayer(
        @Query("apikey") apiKey: String,
        @Query("name") playerName: String?
    ): PlayerFinderDTO
}