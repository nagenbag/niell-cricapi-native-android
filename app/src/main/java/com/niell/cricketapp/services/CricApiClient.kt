package com.niell.cricketapp.services

import com.niell.cricketapp.helpers.BaseRetrofitClient
import com.google.gson.annotations.SerializedName
import okhttp3.Interceptor

class CricApiClient : BaseRetrofitClient() {

    val apiClient = getClient().create(CricApi::class.java)

    override fun getCustomInterceptor(): Interceptor? = null
    override fun getBaseUrl(): String = "https://cricapi.com/api/"
}
