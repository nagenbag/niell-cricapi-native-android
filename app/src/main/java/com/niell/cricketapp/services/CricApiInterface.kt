package com.niell.cricketapp.services

import retrofit2.http.GET
import retrofit2.http.Path

interface CricApiInterface {

    @GET("")
    suspend fun request(
        @Path("") apiKey: String
    )
}