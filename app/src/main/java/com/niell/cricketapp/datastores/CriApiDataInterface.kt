package com.niell.cricketapp.datastores

import com.niell.cricketapp.models.*


interface CriApiDataInterface {
    suspend fun getNewMatches() : NewMatchesDTO

    suspend fun getOldMatches() : OldMatchesDTO

    suspend fun getMatchCalendar() : MatchCalendarDTO

    suspend fun getCricketScore(matchID: String?) : CricketScoreDTO

    suspend fun getPlayerScore(playerID: String?) : PlayerStatisticsDTO

    suspend fun getFindPlayer(playerName: String?) : PlayerFinderDTO
}