package com.niell.cricketapp.datastores

import com.niell.cricketapp.models.*
import com.niell.cricketapp.services.CricApiClient

class CricApiDataStore : CriApiDataInterface {
    companion object {
        private const val CRIC_API_KEY: String = "4Px12qLiOiafLWaJizvmUVnUFFX2"

        private var CricketScore: List<CricketScoreDTO> = listOf()
    }

    private val networkClient = CricApiClient().apiClient

    override suspend fun getNewMatches(): NewMatchesDTO {
        return networkClient.requestNewMatches(CRIC_API_KEY)
    }

    override suspend fun getOldMatches(): OldMatchesDTO {
       return networkClient.requestOldMatches(CRIC_API_KEY)
    }

    override suspend fun getMatchCalendar(): MatchCalendarDTO {
        return networkClient.requestMatchCalendar(CRIC_API_KEY)
    }

    override suspend fun getCricketScore(matchID: String?): CricketScoreDTO {
        return networkClient.requestCricketScore(CRIC_API_KEY, matchID)
    }

    override suspend fun getPlayerScore(playerID: String?): PlayerStatisticsDTO {
        return networkClient.requestPlayerScore(CRIC_API_KEY, playerID)
    }

    override suspend fun getFindPlayer(playerName: String?): PlayerFinderDTO {
        return networkClient.requestFindPlayer(CRIC_API_KEY, playerName)
    }

}