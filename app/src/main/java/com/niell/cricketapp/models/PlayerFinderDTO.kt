package com.niell.cricketapp.models

data class PlayerFinderDTO(
    val data: List<Players>?,
    val v: String?,
    val ttl: String?,
    val provider: ProviderDTO?,
    val creditsLeft: String?,
    val cache3: Boolean?
) {
    data class Players(
        val pid: String?,
        val fullname: String?,
        val name: String?
    )
}