package com.niell.cricketapp.models
import com.google.gson.annotations.SerializedName

data class OldMatchesDTO(
    val data: List<Match>?,
    val v: String?,
    val ttl: String?,
    val provider: ProviderDTO?,
    val creditsLeft: String?,
    val cache2: Boolean?
) {
    data class Match(
        @SerializedName("unique_id")
        val uniqueID: Number?,

        @SerializedName("description")
        val description: String?,

        @SerializedName("title")
        val title: String?
    )
}