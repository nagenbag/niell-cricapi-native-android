package com.niell.cricketapp.models

import com.google.gson.annotations.SerializedName

data class CricketScoreDTO(
    val score: String?,
    val description: String?,
    val matchStarted: Boolean?,
    val team1: String?,
    val team2: String?,
    val v: String?,
    val ttl: String?,
    val provider: ProviderDTO?,
    val creditsLeft: String?
)