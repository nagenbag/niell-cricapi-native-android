package com.niell.cricketapp.models

import com.google.gson.annotations.SerializedName

data class ProviderDTO(
    @SerializedName("source")
    val source: String?,

    @SerializedName("url")
    val url: String?,

    @SerializedName("pubDate")
    val pudDate: String?
)