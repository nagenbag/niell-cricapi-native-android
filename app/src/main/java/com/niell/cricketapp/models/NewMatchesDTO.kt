package com.niell.cricketapp.models

import com.google.gson.annotations.SerializedName

data class NewMatchesDTO(
    val matches: List<Match>?,
    val v: String?,
    val ttl: String?,
    val provider: ProviderDTO?,
    val creditsLeft: String?
) {
    data class Match(
        @SerializedName("unique_id")
        val uniqueID: Number?,

        @SerializedName("date")
        val date: String?,

        @SerializedName("dateTimeGMT")
        val dateTimeGMT: String?,

        @SerializedName("team-1")
        val team1: String?,

        @SerializedName("team-2")
        val team2: String?,

        @SerializedName("squad")
        val squad: Boolean?,

        @SerializedName("toss_winner_team")
        val tossWinnerTeam: String?,

        @SerializedName("winner_team")
        val winningTeam: String?,

        @SerializedName("matchStarted")
        val matchStarted: Boolean?,

        @SerializedName("type")
        val type: String?
    )
}