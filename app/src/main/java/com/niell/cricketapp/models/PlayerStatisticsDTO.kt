package com.niell.cricketapp.models

data class PlayerStatisticsDTO(
    val pid: String?,
    val profile: String?,
    val imageURL: String?,
    val battingStyle: String?,
    val bowlingStyle: String?,
    val majorTeams: String?,
    val currentAge: String?,
    val born: String,
    val fullName: String?,
    val name: String?,
    val country: String?,
    val playingRole: String?,
    val v: String?,
    val ttl: String?,
    val provider: ProviderDTO?,
    val creditsLeft: String?
)