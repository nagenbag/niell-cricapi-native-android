package com.niell.cricketapp.models

import com.google.gson.annotations.SerializedName

data class MatchCalendarDTO(
    val data: List<Match>?,
    val v: String?,
    val ttl: String?,
    val provider: ProviderDTO?,
    val creditsLeft: String?,
    val cache: Boolean?
) {
    data class Match(
        @SerializedName("unique_id")
        val uniqueID: Number?,

        @SerializedName("name")
        val name: String?,

        @SerializedName("date")
        val date: String?
    )
}