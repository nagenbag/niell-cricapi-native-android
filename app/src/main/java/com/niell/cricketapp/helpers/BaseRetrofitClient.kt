package com.niell.cricketapp.helpers

import okhttp3.OkHttpClient
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

abstract class BaseRetrofitClient {
    companion object {
        private const val TIMEOUT: Long = 300
    }

    fun getClient(): Retrofit {
        val client = OkHttpClient.Builder()
        val customInterceptor = getCustomInterceptor()
        customInterceptor?.let {
            client.addInterceptor(it)
        }
        client.addInterceptor(
            getLoggingInterceptor()
        )
        client.readTimeout(TIMEOUT, TimeUnit.SECONDS)
        client.writeTimeout(TIMEOUT, TimeUnit.SECONDS)

        return Retrofit.Builder()
            .baseUrl(getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

    private fun getLoggingInterceptor() : Interceptor {
        // TODO: Update logging level based on build config
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
    }

    abstract fun getCustomInterceptor() : Interceptor?

    abstract fun getBaseUrl() : String
}