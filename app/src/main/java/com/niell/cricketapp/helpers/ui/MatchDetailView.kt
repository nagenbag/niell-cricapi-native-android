package com.niell.cricketapp.helpers.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.niell.cricketapp.R
import com.niell.cricketapp.models.CricketScoreDTO
import kotlinx.android.synthetic.main.view_match_detail.view.*

class MatchDetailView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_match_detail, this, true)
    }

    fun populateMatchDetailView(data: CricketScoreDTO?) {
        if (data != null) {
            txtDescription.text = data.description
            val scoreLabel = "Score: ${data.score}"
            txtScore.text = scoreLabel

            txtDescription.visibility = View.VISIBLE
            txtDescription.visibility = View.VISIBLE
            newMatchesError.visibility = View.GONE

        } else {
            txtDescription.visibility = View.GONE
            txtDescription.visibility = View.GONE
            newMatchesError.visibility = View.VISIBLE
        }
    }
}