package com.niell.cricketapp.helpers

import android.app.Activity
import androidx.fragment.app.Fragment
import com.niell.cricketapp.ui.MainActivity

val Fragment.mainActivity: MainActivity?
    get() = this.activity as? MainActivity