package com.niell.cricketapp.helpers.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.niell.cricketapp.R
import com.niell.cricketapp.models.NewMatchesDTO
import kotlinx.android.synthetic.main.view_new_match_basic.view.*
import java.text.SimpleDateFormat
import java.util.*

class NewMatchBasicView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_new_match_basic, this, true)
    }

    fun populateBasicMatchView(data: NewMatchesDTO.Match?) {
        val displayDateFormat = SimpleDateFormat("dd MMM yyyy hh:mm", Locale.getDefault())
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:m", Locale.getDefault())
        val date = dateFormat.parse(data?.date ?: "")

        txtDate.text = displayDateFormat.format(date)
        txtTeam1.text = data?.team1
        txtTeam2.text = data?.team2
    }
}