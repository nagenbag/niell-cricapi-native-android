package com.niell.cricketapp.helpers.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.niell.cricketapp.R
import com.niell.cricketapp.models.OldMatchesDTO
import kotlinx.android.synthetic.main.view_old_match_basic.view.*

class OldMatchBasicView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_old_match_basic, this, true)
    }

    fun populateBasicMatchView(data: OldMatchesDTO.Match?) {
        txtTitle.text = data?.title
        txtDescription.text = data?.description
    }
}