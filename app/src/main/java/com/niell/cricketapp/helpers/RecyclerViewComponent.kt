package com.niell.cricketapp.helpers

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import androidx.annotation.UiThread
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.niell.cricketapp.adaptors.RecyclerViewAdaptor

class RecyclerViewComponent @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    private val customAdapter = RecyclerViewAdaptor()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        this.apply {
            layoutManager = LinearLayoutManager(context as Activity)
            adapter = customAdapter
        }
    }

    @UiThread
    fun updateList(dataSet: List<BaseCardModel>?) {
        if (dataSet != null) {
            customAdapter.dataSet = dataSet.toMutableList()
        }
    }

    @UiThread
    fun updateAtIndex(index: Int?) {
        if (index != null) {
            customAdapter.notifyItemChanged(index)
        }
    }
}