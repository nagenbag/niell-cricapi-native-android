package com.niell.cricketapp.helpers

import android.view.View
import androidx.recyclerview.widget.RecyclerView


abstract class BaseCardModel(
    val type: CardType
) {
    enum class CardType {
        UNSPECIFIED,

        OLD_MATCH,
        NEW_MATCH,
        CALENDAR_MATCH
    }
}

fun getCardTypeFromOrdinal(ordinal: Int): BaseCardModel.CardType {
   return when (ordinal) {
        BaseCardModel.CardType.OLD_MATCH.ordinal -> BaseCardModel.CardType.OLD_MATCH
        BaseCardModel.CardType.NEW_MATCH.ordinal -> BaseCardModel.CardType.NEW_MATCH
        BaseCardModel.CardType.CALENDAR_MATCH.ordinal -> BaseCardModel.CardType.CALENDAR_MATCH
        else -> BaseCardModel.CardType.UNSPECIFIED
    }
}

open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    open fun bind(item: BaseCardModel) {
        // If there's any base implementation needed for the view holder, bind it here.
    }

    open fun attach() {}

    open fun detach() {}
}