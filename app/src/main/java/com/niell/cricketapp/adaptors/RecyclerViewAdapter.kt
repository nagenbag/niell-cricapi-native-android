package com.niell.cricketapp.adaptors


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.niell.cricketapp.helpers.BaseCardModel
import com.niell.cricketapp.helpers.BaseViewHolder
import com.niell.cricketapp.helpers.getCardTypeFromOrdinal
import com.niell.cricketapp.ui.calendarmatches.CalendarMatchViewholder
import com.niell.cricketapp.ui.matchdetails.NewMatchesViewHolder
import com.niell.cricketapp.ui.matchdetails.OldMatchViewHolder

/**
 * Sets a binding between data and views.
 */
class RecyclerViewAdaptor : RecyclerView.Adapter<BaseViewHolder>() {
    private var _dataSet: MutableList<BaseCardModel> = mutableListOf()
    var dataSet: MutableList<BaseCardModel>
        set(value) {
            _dataSet = value
            this.notifyDataSetChanged()
        }
        get() = _dataSet

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        setViewHolder(holder, position)
    }

    override fun getItemCount(): Int {
        return dataSet.count()
    }

    private fun setViewHolder(holder: BaseViewHolder, position: Int) {
        val item = dataSet[position]
        holder.bind(item)
    }

    override fun onViewAttachedToWindow(holder: BaseViewHolder) {
        holder.attach()
    }

    override fun getItemViewType(position: Int): Int {
        return dataSet[position].type.ordinal
    }

    /**
     * This function is used to return the correct view holder based on the CardType enum.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return getBaseCardViewHolder(viewType, inflater, parent)
    }

    private fun getBaseCardViewHolder(
        viewType: Int,
        inflater: LayoutInflater,
        parent: ViewGroup
    ): BaseViewHolder {
        return when (getCardTypeFromOrdinal(viewType)){
            BaseCardModel.CardType.UNSPECIFIED -> TODO() // Create a UNSPECIDIED VIEWHOLDER
            BaseCardModel.CardType.OLD_MATCH -> OldMatchViewHolder.getInstance(inflater, parent)
            BaseCardModel.CardType.NEW_MATCH -> NewMatchesViewHolder.getInstance(inflater, parent)
            BaseCardModel.CardType.CALENDAR_MATCH -> CalendarMatchViewholder.getInstance(inflater, parent)
        }
    }
}