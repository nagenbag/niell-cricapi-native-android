package com.niell.cricketapp.adaptors

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.niell.cricketapp.R
import kotlinx.android.synthetic.main.grid_item.view.*

data class GridViewItemModel(
    val playerID: String?,
    val playerName: String?,
    val onItemClicked: (playerID: String?)-> Unit
)

class GridViewAdaptor(itemList: List<GridViewItemModel>) : BaseAdapter() {
    val items: List<GridViewItemModel> = itemList

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = this.items[position]
        val inflater = LayoutInflater.from(parent?.context)
        val itemView: View = inflater.inflate(R.layout.grid_item,  parent, false)

        itemView.txtPlayerName.text = item.playerName

        itemView.setOnClickListener {
            item.onItemClicked(item.playerID)
        }

        return itemView
    }
}